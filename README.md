# Aruba Access Point Geolocation Service

##Introduction

This is a restful service that provides a platform to query the geolocation coordinates for a given set of 
wifi access points scanned data. This is necessary for determination of approximate location of wifi access points.

This service depends on google maps api as a downstream service to get the geolocation coordinates, therefore 
GOOGLE_API_KEYS are required as part of the application configuration, which are configurable through the 
environmental variables.

```python
GOOGLE_MAPS_KEY = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
```

##Caching
The service also has comes with geolocation caching on board, which can be helpful in terms of saving costs due to
api calls to google maps services as well as performance enhancements. Caching does come as a double edged sword, it can 
potentially serve us from costs and give us performance gain, on the down side it can significantly affect our service.
We might not necessarily always get fresh results which might lead to our service producing incorrect and stale data.
Given our unique problem, the nature our access point inputs can also affect our caching behaviour, consider the attribute 
rssi, which is very dynamic in nature, also on a set of access point data presence some points of low signal strength is not
guaranteed. All these factors have direct negative consequences to our caching behaviour.
 
Therefore, taking all that into consideration, our caching mechanism is highly configurable with two degrees of freedom,
```python
CACHE_DEFAULT_TIMEOUT = 3600
CACHE_RSSI_THRESHOLD = -81
```

*CACHE_DEFAULT_TIMEOUT* - this is the number of seconds before we expire cached data to prevent stale data
*CACHE_RSSI_THRESHOLD* - this is the rssi value that we can tolerate when caching access points, if its lower than this
it means the signal is to low to always guarantee the presence of that data point so its best to exclude it during caching.
This link https://www.metageek.com/training/resources/understanding-rssi.html could be helpful in determining the best possible.

By fine tuning these variables well, we will have the better caching behaviour.We can always turn caching off as below
```python
CACHE_ENABLED = False
```


Our caching is performed through hashing of access points set data which will form a unique **key** for that set, and the **value**
will be the geolocation coordinates. Therefore, our caching is as good as our hash function. Ideally, a hash function should be a one to one
mapping function otherwise it will lead to incorrect results. For simplicity, the default python hash function has been used.
A better hash function could be used as an improvement to this solution.

Caching infrastructure is also an important issue, for this service, **simple** in memory cache has been used for simplicity, we can easily onboard
other robust caching database infrastructure such as reddis and memcached easily, that can be done through environmental variables, and very minor code changes
checkout the following link https://flask-caching.readthedocs.io/en/latest/#memcachedcache


##Monitoring
The service does have sentry on board, which can be configured through environmental variables if needed.
Its currently not being used
  

## Development

### Make commands

Google api keys: `export GOOGLE_MAPS_KEY="*****************************"`

Initial installation: `make install`

To run test: `make tests`

To run application: `make run`

To run all above commands at once: `make all`

### Other commands

To open an interactive shell: `python manage.py shell`

See supported commands: `python manage.py --help`

### Viewing the app ###

Open the following url on your browser to view swagger documentation
http://127.0.0.1:5000/

## Production

### Running on Linux distributions

#### Prerequisites

1. [Virtualenv](https://virtualenv.pypa.io/en/stable/installation/)

#### Installation

```bash
$ export GOOGLE_MAPS_KEY="*****************************"
$ python_version=python3.6
$ virtualenv --python=${python_version} .
$ source bin/activate
$ pip install -r requirements.txt
$ make run

```


### Running on docker container

Build image, and run as normal

See example environment config in [.env.production.tpl](./.env.production.tpl)

See available configuration in [config.py](./app/main/config.py) and [gunicorn.conf](./gunicorn.conf)


##References
https://github.com/pmint93/flask-restplus-boilerplate
