from flask_restplus import Api, Namespace
from flask import Blueprint
from flask_cors import CORS

from .main.controller.health_controller   import api as health_namespace

from .main.controller.access_point_location_controller import api as access_point_location

blueprint = Blueprint('api', __name__)

# CORS(blueprint, origins=['*'], allow_headers=['*'], expose_headers=['*'])

api = Api(blueprint,
          title='AP Geolocation Service',
          version='1.0',
          description='Access Point Geolocation service'
          )

api.add_namespace(health_namespace, path='/health')

api.add_namespace(access_point_location, path ='/access_point_location')
