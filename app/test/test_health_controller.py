from base import BaseTestCase


class HealthCheckAPI(BaseTestCase):

    def test_health_endpoint(self):
        response = self.client.get("/health")
        self.assert200(response)
