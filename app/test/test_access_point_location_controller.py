import json
from base import BaseTestCase
from werkzeug.exceptions import HTTPException


class GetApLocationAPI(BaseTestCase):
    def test_validation_fails_if_no_inputs_supplied(self):
        response = self.client.post("/access_point_location")
        assert response.status_code == 400

    def test_fails_validation_on_incorrect_inputs(self):
        random_input = {'foo': 'bar'}
        response = self.client.post("/access_point_location", data=random_input,
                                    content_type='application/json')

        assert response.status_code == 400












