import requests
from base import BaseTestCase
from flask import abort
from werkzeug.exceptions import ServiceUnavailable
from app.main.service.geolocation import GeolocationService


class GeolocationServiceTest(BaseTestCase):
    apscan_data_sample = {
        'apscan_data': [
            {
                'bssid': 'foo',
                'rssi': 0,
                'channel': 'bar'
            }
        ]
    }

    valid_response = {
        "location": {
            "lat": 0,
            "lng": 0
        },
        "accuracy": 0
    }
    known_error = {
                 "error": {
                      "errors": [
                       {
                        "domain": "global",
                        "reason": "parseError",
                        "message": "Parse Error",
                       }
                      ],
                      "code": 400,
                      "message": "Parse Error"
                     }
            }

    def test_build_geolocation_request(self):
        geo_request = GeolocationService.build_geolocation_request(self.apscan_data_sample)
        expected_geo_request = {
            "considerIp": "false",
            "wifiAccessPoints": [
                {
                    'macAddress': self.apscan_data_sample['apscan_data'][0]['bssid'],
                    'signalStrength': self.apscan_data_sample['apscan_data'][0]['rssi'],
                    'channel': self.apscan_data_sample['apscan_data'][0]['channel']
                }
            ]
        }
        assert geo_request == expected_geo_request

    def test_get_ap_geolocation_from_cache(self):
        self.app.config['CACHE_ENABLED'] = True
        expected_cache_key = hash(self.apscan_data_sample['apscan_data'][0]['bssid'])
        self.spy_on(self.app.cache.get)

        try:
            geolocation_service = GeolocationService()
            geolocation_service.get_ap_location(self.apscan_data_sample)
        except Exception:
            pass

        self.assertTrue(self.app.cache.get.called)
        self.assertTrue(self.app.cache.get.called_with(expected_cache_key))

        self.app.cache.get.unspy()

    def test_cache_ap_geolocation(self):
        class MockResponse:
            json_data = self.valid_response
            status_code = 200

            def json(self):
                return self.json_data

        def fake_request(url, data={}, json=None, **kwargs):
            return MockResponse()

        expected_cache_key = hash(self.apscan_data_sample['apscan_data'][0]['bssid'])

        self.spy_on(self.app.cache.set)
        self.spy_on(requests.post, call_fake=fake_request)

        geolocation_service = GeolocationService()
        geolocation_service.get_ap_location(self.apscan_data_sample)

        self.assertTrue(self.app.cache.set.called)
        self.assertTrue(self.app.cache.set.called_with(expected_cache_key, self.valid_response))

        self.app.cache.set.unspy()
        requests.post.unspy()

    def test_no_caching_ap_geolocation_when_disabled(self):
        class MockResponse:
            json_data = self.valid_response
            status_code = 200

            def json(self):
                return self.json_data

        def fake_request(url, data={}, json=None, **kwargs):
            return MockResponse()

        self.app.config['CACHE_ENABLED'] = False

        expected_cache_key = hash(self.apscan_data_sample['apscan_data'][0]['bssid'])

        self.spy_on(self.app.cache.set)

        self.spy_on(requests.post, call_fake=fake_request)

        geolocation_service = GeolocationService()
        geolocation_service.get_ap_location(self.apscan_data_sample)

        self.assertTrue(self.app.cache.set.called is False)
        self.assertTrue(self.app.cache.set.called_with(expected_cache_key, self.valid_response) is False)

        self.app.cache.set.unspy()
        requests.post.unspy()

    def test_handle_unknown_downstream_error(self):

        geolocation_service = GeolocationService()
        self.assertRaises(ServiceUnavailable, geolocation_service.get_ap_location, self.apscan_data_sample)

    def test_handle_known_errors(self):
        class MockResponse:
            json_data = self.known_error
            status_code = 400

            def json(self):
                return self.json_data

        def fake_request(url, data={}, json=None, **kwargs):
            return MockResponse()

        self.spy_on(requests.post, call_fake=fake_request)
        self.spy_on(abort)

        try:
            geolocation_service = GeolocationService()
            geolocation_service.get_ap_location(self.apscan_data_sample)
        except ServiceUnavailable:
            pass

        self.assertTrue(
            abort.called_with(503,
                              [{
                                'error': self.known_error['error']['errors'][0]['reason'],
                                'description': 'The request body is not valid JSON',
                                }]
                              ))

        requests.post.unspy()
        abort.unspy()












