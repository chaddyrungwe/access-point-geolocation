
from flask_testing import TestCase
from kgb import SpyAgency
from manage import app


class BaseTestCase(SpyAgency, TestCase):
    """ Base Tests """

    def create_app(self):
        app.config.from_object('app.main.config.TestingConfig')
        return app

    def setUp(self):
        pass

    def tearDown(self):
        self.app.cache.clear()

