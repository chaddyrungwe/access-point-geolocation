from flask_restplus import  fields

ap_scan_model_properties = {
    'bssid': fields.String(required=True),
    'rssi': fields.Integer(required=True),
    'channel': fields.String(required=True),
}
