import requests
from flask import current_app as app, abort


class GeolocationService(object):

    def get_ap_location(self, apscan_data):
        cache_enabled = app.config['CACHE_ENABLED']
        cached_response = self._get_geolocation_from_cache(apscan_data)

        if cache_enabled is True and cached_response is not None:
            return cached_response

        geolocation_request = self.build_geolocation_request(apscan_data)

        api_key = app.config['GOOGLE_MAPS_KEY']

        response = requests.post(
            url='https://www.googleapis.com/geolocation/v1/geolocate?key='+api_key,
            json=geolocation_request,
        )
        if response.status_code == 200:
            if cache_enabled is True:
                self._cache_ap_geolocation(apscan_data, response.json())
            return response.json()
        else:
            self._build_error_response(response.json()['error']['errors'])

    @staticmethod
    def build_geolocation_request(apscan_data):
        geolocation_request = {
            "considerIp": "false",
            "wifiAccessPoints": []
        }

        for ap in apscan_data['apscan_data']:
            geolocation_request['wifiAccessPoints'].append({
                'macAddress': ap['bssid'],
                'signalStrength': ap['rssi'],
                'channel': ap['channel']
            })
        return geolocation_request

    @staticmethod
    def _get_geolocation_from_cache(apscan_data):
        rssi_threshold = app.config['CACHE_RSSI_THRESHOLD']
        bssid_hash = 0

        for ap in apscan_data['apscan_data']:
            if ap['rssi'] > rssi_threshold:
                bssid_hash += hash(ap['bssid'])
        return app.cache.get(bssid_hash)

    @staticmethod
    def _cache_ap_geolocation(apscan_data, geolocation):
        rssi_threshold = app.config['CACHE_RSSI_THRESHOLD']
        bssid_hash = 0

        for ap in apscan_data['apscan_data']:
            if ap['rssi'] > rssi_threshold:
                bssid_hash += hash(ap['bssid'])
        app.cache.set(bssid_hash, geolocation)


    @staticmethod
    def _build_error_response(errors):
        error_list = []
        for error in errors:
            if error['reason'] == 'dailyLimitExceeded':
                error_list.append({
                    'error': 'dailyLimitExceeded',
                    'description': 'Google maps geolocation api daily limit has been reached'
                })
            elif error['reason'] == 'keyInvalid':
                error_list.append({
                    'error': 'keyInvalid',
                    'description': 'Invalid google maps geolocation api key'
                })
            elif error['reason'] == 'userRateLimitExceeded':
                error_list.append({
                    'error': 'userRateLimitExceeded',
                    'description': 'Configured google maps geolocation api daily limit has been reached'
                })
            elif error['reason'] == 'notFound':
                error_list.append({
                    'error': 'notFound',
                    'description': 'The request was valid, but no results were returned'
                })
            elif error['reason'] == 'parseError':
                error_list.append({
                    'error': 'parseError',
                    'description': 'The request body is not valid JSON'
                })

        if len(error_list) > 0:
            abort(503, error_list)

        else:
            abort(503, [{
                'error': 'Unknown',
                'description': 'An unexpected error occurred'
            }])



