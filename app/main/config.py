import os

class Config:
    SECRET_KEY = os.getenv('SECRET_KEY', 'my_super_strong_secret_key_hehe')
    DEBUG = False
    CACHE_TYPE = os.getenv('CACHE_TYPE', 'simple')
    CACHE_DEFAULT_TIMEOUT = os.getenv('CACHE_DEFAULT_TIMEOUT', 3600)
    CACHE_ENABLED = os.getenv('CACHE_ENABLED', True)
    CACHE_RSSI_THRESHOLD = os.getenv('CACHE_RSSI_THRESHOLD', -80)
    # Sentry config
    SENTRY_CONFIG = {
        'dsn': os.getenv('SENTRY_CONFIG_DSN', 'testing')
    } if os.getenv('SENTRY_CONFIG', 'False').lower() != 'false' else False
    GOOGLE_MAPS_KEY = os.getenv('GOOGLE_MAPS_KEY', 'some-random-key')


class DevelopmentConfig(Config):
    DEBUG = True


class TestingConfig(Config):
    DEBUG = True
    TESTING = True


class ProductionConfig(Config):
    DEBUG = False


config_by_name = dict(
    development=DevelopmentConfig,
    test=TestingConfig,
    production=ProductionConfig
)
