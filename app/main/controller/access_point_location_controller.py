from flask import request, Response
from flask_restplus import Resource, Namespace, fields
from ..dto.access_point_scan_dto import ap_scan_model_properties
from ..service.geolocation import GeolocationService


api = Namespace('access_point_location', description='access point location endpoint')


ap_scan_model = api.model('ap_scan_data', {
    'apscan_data': fields.List(
        fields.Nested(api.model('ap_scan_data_properties', ap_scan_model_properties))
    ),
})

coordinates = api.model('coordinates', {
    'lat': fields.Float(required=True),
    'lng': fields.Float(required=True)
})
geolocation_model = api.model('location', {
    'location': fields.Nested(coordinates),
    'accuracy': fields.Float(required=True)
})

geolocation_service = GeolocationService()


@api.route('')
class GetApLocationAPI(Resource):

    @api.marshal_with(geolocation_model)
    @api.expect(ap_scan_model, validate=True )
    def post(self):
        """Access Point Location API"""
        response = geolocation_service.get_ap_location(request.json)

        return response
